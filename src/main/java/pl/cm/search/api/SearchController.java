package pl.cm.search.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.cm.search.model.User;
import pl.cm.search.repository.UserRepository;

@RestController
@RequestMapping("/api")
public class SearchController {

  private final UserRepository userRepository;

  @Autowired
  public SearchController(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @GetMapping("user/search")
  public Iterable<User> getAllUsers() {
    return userRepository.findAll();
  }

}
