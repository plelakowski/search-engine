package pl.cm.search.repository;

import org.springframework.data.repository.CrudRepository;

import pl.cm.search.model.User;

public interface UserRepository extends CrudRepository<User,Long> {
}
